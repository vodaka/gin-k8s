package util

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"unicode"
)

func Handle_Memory(memory int64) float64 {
	return float64(memory / 1024 / 1024)
}

func Handle_Disk(disk int64) float64 {
	return float64(disk / 1024 / 1024 / 1024)
}

func isDigit(str string) bool {
	for _, x := range []rune(str) {
		if !unicode.IsDigit(x) {
			return false
		}
	}
	return true
}

func Parse_Cpu(cpu string) float64 {
	if cpu == "0" || cpu == "" {
		return 0
	} else if isDigit(cpu) {
		temp, _ := strconv.ParseFloat(cpu, 64)
		return float64(temp * 1000)
	} else if strings.HasSuffix(cpu, "m") {
		s := strings.Split(cpu, "m")
		temp, _ := strconv.ParseFloat(s[0], 64)
		return float64(temp)
	} else if strings.HasSuffix(cpu, "n") {
		s := strings.Split(cpu, "n")
		temp, _ := strconv.ParseFloat(s[0], 64)
		return math.Ceil(float64(temp / 1000 / 1000))
	} else if strings.HasSuffix(cpu, "u") {
		s := strings.Split(cpu, "u")
		temp, _ := strconv.ParseFloat(s[0], 64)
		return math.Ceil(float64(temp / 1000 / 1000 / 1000))
	} else {
		fmt.Printf("解析cpu: %s出现未知类型\n", cpu)
		return 0
	}
}
func Parse_Memory(memory string) float64 {
	if memory == "0" || memory == "" {
		return 0
	} else if strings.HasSuffix(memory, "Ki") {
		s := strings.Split(memory, "Ki")
		temp, _ := strconv.ParseFloat(s[0], 64)
		return math.Ceil(float64(temp / 1024))
	} else if strings.HasSuffix(memory, "Mi") {
		s := strings.Split(memory, "Mi")
		temp, _ := strconv.ParseFloat(s[0], 64)
		return float64(temp)
	} else if strings.HasSuffix(memory, "Gi") {
		s := strings.Split(memory, "Gi")
		temp, _ := strconv.ParseFloat(s[0], 64)
		return math.Ceil(float64(temp * 1024))
	} else {
		fmt.Printf("解析memory: %s出现未知类型\n", memory)
		return 0
	}
}
