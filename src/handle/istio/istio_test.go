package istio

import (
	"fmt"
	"testing"

	"gitee.com/cmlfxz/gin-k8s/src/lib/httpclient"
)

var (
	get_gateway_list = "http://192.168.11.5:8090/k8s/get_gateway_list"
)

func Test_Istio(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := make(map[string]interface{})
	data["namespace"] = "ms-prod"
	result, err := httpclient.Post(get_gateway_list, data, headers)
	fmt.Println(result)
	fmt.Println(string(result), err)
}
