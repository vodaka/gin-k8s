package service

import (
	"fmt"
	"testing"

	"gitee.com/cmlfxz/gin-k8s/src/lib/httpclient"
)

var (
	get_service_list = "http://192.168.11.5:8090/k8s/get_service_list"
	get_ingress_list = "http://192.168.11.5:8090/k8s/get_ingress_list"
)

func Test_List(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := make(map[string]interface{})
	data["namespace"] = "all"
	result, err := httpclient.Post(get_service_list, data, headers)
	fmt.Println(string(result), err)
}

func Test_Ingress(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := make(map[string]interface{})
	data["namespace"] = "all"
	result, err := httpclient.Post(get_ingress_list, data, headers)
	fmt.Println(string(result), err)
}
