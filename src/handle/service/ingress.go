package service

import (
	"context"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"
	// "k8s.io/api/extensions/v1beta1"
	v1 "k8s.io/api/networking/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

// func Get_Ingress_List(c *gin.Context) {
// 	var data ListData
// 	if err := c.ShouldBindJSON(&data); err != nil {
// 		c.JSON(500, gin.H{"fail": "传参错误"})
// 		return
// 	}
// 	namespace := data.Namespace
// 	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

// 	if err != nil {
// 		c.JSON(500, gin.H{"fail": "k8s链接失败"})
// 		return
// 	}
// 	client := clientset.ExtensionsV1beta1()
// 	var ingresss *v1beta1.IngressList
// 	if namespace == "all" || namespace == "" {
// 		ingresss, _ = client.Ingresses("").List(context.TODO(), metaV1.ListOptions{})
// 	} else {
// 		ingresss, _ = client.Ingresses(namespace).List(context.TODO(), metaV1.ListOptions{})
// 	}

// 	list := make([]map[string]interface{}, 0)
// 	for _, ingress := range ingresss.Items {
// 		meta := ingress.ObjectMeta
// 		name := meta.Name
// 		namespace := meta.Namespace
// 		create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
// 		spec := ingress.Spec
// 		rules := spec.Rules
// 		domain_list := make([]string, 0)
// 		for _, rule := range rules {
// 			domain := rule.Host
// 			domain_list = append(domain_list, domain)
// 		}
// 		tls := spec.TLS

// 		detail := map[string]interface{}{
// 			"name":        name,
// 			"namespace":   namespace,
// 			"domain_list": domain_list,
// 			"rules":       rules,
// 			"tls":         tls,
// 			"create_time": create_time,
// 		}
// 		list = append(list, detail)
// 	}
// 	c.JSON(200, list)
// }

func Get_Ingress_List(c *gin.Context) {
	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.NetworkingV1()
	var ingresss *v1.IngressList
	if namespace == "all" || namespace == "" {
		ingresss, _ = client.Ingresses("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		ingresss, _ = client.Ingresses(namespace).List(context.TODO(), metaV1.ListOptions{})
	}

	list := make([]map[string]interface{}, 0)
	fmt.Println(ingresss)
	for _, ingress := range ingresss.Items {
		meta := ingress.ObjectMeta
		name := meta.Name
		namespace := meta.Namespace
		create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
		spec := ingress.Spec
		rules := spec.Rules
		domain_list := make([]string, 0)
		for _, rule := range rules {
			domain := rule.Host
			domain_list = append(domain_list, domain)
		}
		tls := spec.TLS

		detail := map[string]interface{}{
			"name":        name,
			"namespace":   namespace,
			"domain_list": domain_list,
			"rules":       rules,
			"tls":         tls,
			"create_time": create_time,
		}
		list = append(list, detail)
	}
	c.JSON(200, list)
}

func Delete_Ingress(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参有误"})
	} else {
		namespace := data.Namespace
		name := data.Name
		clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
		if err != nil {
			c.JSON(500, gin.H{"fail": "k8s连接不上"})
		} else {
			client := clientset.ExtensionsV1beta1()
			if namespace == "all" || namespace == "" {
				c.JSON(500, gin.H{"fail": "namespace不能为空，并且不能选择all"})
			} else {
				err := client.Ingresses(namespace).Delete(context.TODO(), name, metaV1.DeleteOptions{})
				if err == nil {
					c.JSON(200, gin.H{"ok": "删除成功"})
				} else {
					c.JSON(500, gin.H{"fail": "删除失败"})
				}
			}
		}
	}
}
