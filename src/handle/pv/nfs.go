package pv

import (
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type Storage struct {
	Name             string                        `json:"name"`
	Capacity         string                        `json:"capacity"`
	AccessModes      v1.PersistentVolumeAccessMode `json:"accessModes"`
	ReclaimPolicy    string                        `json:"reclaimPolicy"`
	StorageClassName string                        `json:"storage_class_name"`
	Source           string                        `json:"source"`
}

type NFS struct {
	Path     string `json:"path"`
	Server   string `json:"server"`
	ReadOnly bool   `json:"readonly"`
}
type NFSStorage struct {
	Storage
	Nfs NFS `json:"nfs"`
}

// https://vimsky.com/examples/detail/golang-ex-k8s.io.kubernetes.pkg.client-Client-PersistentVolumes-method.html
func (s *NFSStorage) Create(name string) *v1.PersistentVolume {
	ac := make([]v1.PersistentVolumeAccessMode, 0)
	ac = append(ac, s.AccessModes)

	cap := v1.ResourceList{v1.ResourceName("storage"): resource.MustParse(s.Capacity)}
	spec := v1.PersistentVolumeSpec{
		AccessModes: ac,
		// type ResourceList map[ResourceName]resource.Quantity
		Capacity:                      cap,
		PersistentVolumeReclaimPolicy: v1.PersistentVolumeReclaimPolicy(s.ReclaimPolicy),
		PersistentVolumeSource: v1.PersistentVolumeSource{
			NFS: &v1.NFSVolumeSource{
				Path:     s.Nfs.Path,
				Server:   s.Nfs.Server,
				ReadOnly: s.Nfs.ReadOnly,
			},
		},
		StorageClassName: s.StorageClassName,
	}
	pv := v1.PersistentVolume{
		ObjectMeta: metaV1.ObjectMeta{Name: name},
		TypeMeta:   metaV1.TypeMeta{Kind: "PersistentVolume", APIVersion: "v1"},
		Spec:       spec,
	}

	return &pv

}
