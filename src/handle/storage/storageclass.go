package storage

import (
	"context"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/storage/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func Get_StorageClass_List(c *gin.Context) {
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.StorageV1()
	var storageclasss *v1.StorageClassList
	storageclasss, _ = client.StorageClasses().List(context.TODO(), metaV1.ListOptions{})

	list := make([]map[string]interface{}, 0)
	for _, sc := range storageclasss.Items {
		meta := sc.ObjectMeta
		name := meta.Name
		namespace := meta.Namespace
		create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
		mount_options := sc.MountOptions
		parameters := sc.Parameters
		provisioner := sc.Provisioner
		reclaim_policy := sc.ReclaimPolicy

		volume_binding_mode := sc.VolumeBindingMode

		detail := map[string]interface{}{
			"name":                name,
			"namespace":           namespace,
			"provisioner":         provisioner,
			"reclaim_policy":      reclaim_policy,
			"parameters":          parameters,
			"mount_options":       mount_options,
			"volume_binding_mode": volume_binding_mode,
			"create_time":         create_time,
		}
		list = append(list, detail)
	}
	c.JSON(200, list)
}
