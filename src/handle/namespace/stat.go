package namespace

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"

	pod "gitee.com/cmlfxz/gin-k8s/src/handle/pod"
	"gitee.com/cmlfxz/gin-k8s/src/lib/gredis"
	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

var (
	Redis_Key_TimeOut int = 15 * 60
)

func get_namespace_name_list() ([]string, error) {
	list := make([]string, 0)
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		return nil, errors.New("k8s连接不上")
	}
	client := clientset.CoreV1()
	namespaces, _ := client.Namespaces().List(context.TODO(), metaV1.ListOptions{})
	for _, item := range namespaces.Items {
		name := item.ObjectMeta.Name
		list = append(list, name)
	}
	return list, nil
}

func Get_ALL_Namespace_Resources(c *gin.Context) {
	namespaces, err := get_namespace_name_list()
	if err != nil {
		c.JSON(500, gin.H{"fail": "获取命名空间失败", "error": err})
		return
	}
	client := gredis.NewRedisClient(0)
	cluster_name := c.Request.Header.Get("cluster_name")
	if cluster_name == "" {
		c.JSON(500, gin.H{"fail": "header无法获取集群名称"})
		return
	}
	key := fmt.Sprintf("%s_all_namespace_resources", cluster_name)

	all_namespace_resources, err := client.Get(key).Result()
	fmt.Println(err)
	list := make([]map[string]interface{}, 0)
	if all_namespace_resources == "" {
		for _, namespace := range namespaces {

			resources, _ := pod.Namespace_Resources_Stat(namespace)
			detail := map[string]interface{}{
				"namespace": namespace,
				"resources": resources,
			}
			list = append(list, detail)
		}
		jsonStr, _ := json.Marshal(list)
		value := string(jsonStr)
		client.Set(key, value, time.Duration(Redis_Key_TimeOut))
		all_namespace_resources = value
	}
	c.String(200, all_namespace_resources)

}
