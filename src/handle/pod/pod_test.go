package pod

import (
	"fmt"
	"testing"

	"gitee.com/cmlfxz/gin-k8s/src/lib/httpclient"
)

var (
	get_pod_detail          = "http://192.168.11.5:8090/k8s/get_pod_detail"
	get_namespaced_pod_list = "http://192.168.11.5:8090/k8s/get_namespaced_pod_list"
	get_pod_log             = "http://192.168.11.5:8090/k8s/get_pod_log"
)

// func Test_Pod_Detail(t *testing.T) {
// 	headers := make(map[string]string)
// 	headers["cluster_name"] = "k3s_112"
// 	data := map[string]interface{}{
// 		"name": "flask-admin-8784ccf8b-mjlgj",
// 		// "name":      "flask-admin-xxx",
// 		"namespace": "ms-test",
// 	}
// 	result, err := httpclient.Post(get_pod_detail, data, headers)
// 	fmt.Println(string(result), err)
// }


// func timer1() {
// 	timer := time.NewTicker(10 * time.Second)
// 	select {
// 	case <-timer.C:
// 		namespace := "ms-test"
// 		name := "flask-admin-8784ccf8b-f5tjg"
// 		container := "flask-admin"
// 		result, err := pod_log(namespace, name, container)
// 		fmt.Println(result, err)
// 	}
// }
// func Test_Pod_Log(t *testing.T) {
// 	namespace := "ms-test"
// 	name := "flask-admin-8784ccf8b-f5tjg"
// 	container := "flask-admin"
// 	result, err := pod_log(namespace, name, container)
// 	fmt.Println(result, err)
// 	// go timer1()
// }

func Test_Pod_Log(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := map[string]interface{}{
		"namespace": "ms-test",
		"name":      "flask-admin-8784ccf8b-f5tjg",
		"container": "filebeat",
	}
	result, err := httpclient.Post(get_pod_log, data, headers)
	fmt.Println(string(result), err)
}

// func Test_Namespaced_Pod_List(t *testing.T) {
// 	headers := make(map[string]string)
// 	headers["cluster_name"] = "k3s_112"
// 	data := map[string]interface{}{
// 		"namespace": "ms-test",
// 		"pageSize":  10,
// 		"page":      1,
// 	}
// 	result, err := httpclient.Post(get_namespaced_pod_list, data, headers)
// 	fmt.Println(string(result), err)
// }

// func Test_Parse_Cpu(t *testing.T) {
// 	c := "1"
// 	fmt.Println(Parse_Cpu(c))
// 	c = "1000m"
// 	fmt.Println(Parse_Cpu(c))
// 	c = "1000000n"
// 	fmt.Println(Parse_Cpu(c))
// 	c = "1000000000u"
// 	fmt.Println(Parse_Cpu(c))
// 	c = "133"
// 	fmt.Println(Parse_Cpu(c))
// 	c = "0"
// 	fmt.Println(Parse_Cpu(c))
// 	c = "1024000n"
// 	fmt.Println(Parse_Cpu(c))
// }

// func Test_Parse_memory(t *testing.T) {
// 	m := "1"
// 	fmt.Println(Parse_Memory(m))
// 	m = "1000Ki"
// 	fmt.Println(Parse_Memory(m))
// 	m = "1Mi"
// 	fmt.Println(Parse_Memory(m))
// 	m = "1Gi"
// 	fmt.Println(Parse_Memory(m))
// 	m = "1.5Gi"
// 	fmt.Println(Parse_Memory(m))
// 	m = "0"
// 	fmt.Println(Parse_Memory(m))
// 	m = "20Ki"
// 	fmt.Println(Parse_Memory(m))
// }
