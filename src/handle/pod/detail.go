package pod

import (
	"fmt"
	"strings"

	"gitee.com/cmlfxz/gin-k8s/src/handle/metric"
	v1 "k8s.io/api/core/v1"
)

func Namespace_Pod_Detail(pod *v1.Pod) map[string]interface{} {
	meta := pod.ObjectMeta
	name := meta.Name
	namespace := meta.Namespace
	create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")

	spec := pod.Spec
	node := spec.NodeName
	status := pod.Status
	pod_ip := status.PodIP
	container_status := status.Phase
	var restart_count int32 = 0
	if len(status.ContainerStatuses) > 0 {
		for _, item := range status.ContainerStatuses {
			if strings.Contains(name, item.Name) {
				restart_count = item.RestartCount
				if item.State.Waiting != nil {
					container_status = v1.PodPhase(item.State.Waiting.Reason)
				} else if item.State.Terminated != nil {
					container_status = v1.PodPhase(item.State.Terminated.Reason)
				} else {
					fmt.Println("nothing to do")
				}
			}
		}
	}
	containers := pod.Spec.Containers
	image := containers[0].Image
	resources := Pod_Resource(&containers)

	pod_usage, _ := metric.Get_Pod_Usage(namespace, name)
	detail := map[string]interface{}{
		"name":          name,
		"namespace":     namespace,
		"node":          node,
		"pod_ip":        pod_ip,
		"status":        container_status,
		"restart_count": restart_count,
		"image":         image,
		"resources":     resources,
		"pod_usage":     pod_usage,
		"create_time":   create_time,
	}
	return detail
}
