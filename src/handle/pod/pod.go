package pod

import (
	"context"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/handle/cluster"
	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type PodDetailInput struct {
	Namespace string `json:"namespace" binding:"required"`
	Name      string `json:"name" binding:"required"`
}

func Get_Pod_Detail(c *gin.Context) {
	var data PodDetailInput
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
		return
	}
	client := clientset.CoreV1()

	pod, err := client.Pods(namespace).Get(context.TODO(), name, metaV1.GetOptions{})
	if err != nil {
		c.JSON(500, gin.H{"fail": "找不到pod相关信息"})
		return
	}
	meta := pod.ObjectMeta
	create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
	pod_labels := meta.Labels

	spec := pod.Spec
	var node_affinity *v1.NodeAffinity
	var pod_affinity *v1.PodAffinity
	var pod_anti_affinity *v1.PodAntiAffinity
	affinity := spec.Affinity
	if affinity != nil {
		node_affinity = affinity.NodeAffinity
		pod_affinity = affinity.PodAffinity
		pod_anti_affinity = affinity.PodAntiAffinity
	}

	host_network := spec.HostNetwork
	image_pull_secrets := spec.ImagePullSecrets
	node_selector := spec.NodeSelector
	restart_policy := spec.RestartPolicy
	service_account_name := spec.ServiceAccountName
	tolerations := spec.Tolerations
	containers := spec.Containers
	volumes := spec.Volumes
	// volume_list = get_volumes(volumes)
	init_containers := spec.InitContainers

	status := pod.Status
	phase := status.Phase
	host_ip := status.HostIP
	pod_ip := status.PodIP
	event_list, err := cluster.Get_Resource_Event_List(namespace, "Pod", name)
	if err != nil {
		fmt.Println(err)
		event_list = []map[string]interface{}{}
	}
	detail := map[string]interface{}{
		"name":               name,
		"namespace":          namespace,
		"node":               host_ip,
		"pod_ip":             pod_ip,
		"pod_labels":         pod_labels,
		"status":             phase,
		"create_time":        create_time,
		"affinity":           affinity,
		"nodeAffinity":       node_affinity,
		"podAffinity":        pod_affinity,
		"podAntiAffinity":    pod_anti_affinity,
		"hostNetwork":        host_network,
		"imagePullSecrets":   image_pull_secrets,
		"nodeSelector":       node_selector,
		"restartPolicy":      restart_policy,
		"serviceAccountName": service_account_name,
		"tolerations":        tolerations,
		"volumes":            volumes,
		"containers":         containers,
		"initContainers":     init_containers,
		"event_list":         event_list,
	}
	c.JSON(200, detail)
}

type NamespacePodData struct {
	Namespace string `json:"namespace"`
	PageSize  int    `json:"pageSize"`
	Page      int    `json:"page"`
}

func Get_Namespaced_Pod_List(c *gin.Context) {
	var data NamespacePodData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}

	pods, err := namespaced_pods(data.Namespace)

	if err != nil {
		c.JSON(500, gin.H{"fail": "获取pod数据出错", "error": err})
		return
	}
	result := page_pods_v2(pods, data.PageSize, data.Page)
	c.JSON(200, result)
}

func Get_Problem_Pod_List(c *gin.Context) {
	var data NamespacePodData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	pods, err := problem_pods(data.Namespace)
	if err != nil {
		c.JSON(500, gin.H{"fail": "获取pod数据出错", "error": err})
		return
	}
	result := page_pods(pods, data.PageSize, data.Page)
	c.JSON(200, result)
}

type NodePodData struct {
	Node     string `json:"node"`
	PageSize int    `json:"pageSize"`
	Page     int    `json:"page"`
}

func Get_Pod_List_By_Node(c *gin.Context) {
	var data NodePodData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	pods, err := node_pods(data.Node)
	if err != nil {
		c.JSON(500, gin.H{"fail": "获取pod数据出错", "error": err})
		return
	}
	result := page_pods_v2(pods, data.PageSize, data.Page)
	c.JSON(200, result)
}

type InputData struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`
}

func Delete_POD(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
		return
	}
	client := clientset.CoreV1()
	if namespace == "all" || namespace == "" {
		c.JSON(500, gin.H{"fail": "namespace不能为空，并且不能选择all"})
		return
	}
	err = client.Pods(namespace).Delete(context.TODO(), name, metaV1.DeleteOptions{})
	if err == nil {
		c.JSON(200, gin.H{"ok": "删除成功"})
	} else {
		c.JSON(500, gin.H{"fail": "删除失败"})
	}

}

func Get_Container_Name(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
		return
	}
	client := clientset.CoreV1()
	pod, _ := client.Pods(namespace).Get(context.TODO(), name, metaV1.GetOptions{})

	containers := pod.Spec.Containers
	list := make([]string, 0)
	for _, c := range containers {
		n := c.Name
		list = append(list, n)
	}
	c.JSON(200, list)
}
