package deployment

import (
	// . "gitee.com/cmlfxz/gin-k8s/conf"
	//
	"fmt"

	"github.com/gin-gonic/gin"
)

type ReplicasData struct {
	InputData
	Replicas int32 `json:"replicas"`
}

func Update_Replicas(c *gin.Context) {
	var data ReplicasData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	d := Deploy{Namespace: data.Namespace, Name: data.Name}
	if ok := d.update_replicas(data.Replicas); ok {
		c.JSON(200, gin.H{"ok": "更新成功"})
	} else {
		c.JSON(500, gin.H{"fail": "更新失败"})
	}
}

func Delete_Node_Affinity(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	d := Deploy{Namespace: data.Namespace, Name: data.Name}
	if err := d.delete_node_affinity(); err == nil {
		c.JSON(200, gin.H{"ok": "删除成功"})
	} else {
		c.JSON(500, gin.H{"fail": "删除失败", "error": err.Error()})
	}
}

type Node_Affinity_Input struct {
	Namespace    string             `json:"namespace"`
	Name         string             `json:"name"`
	NodeAffinity Node_Affinity_Data `json:"node_affinity"`
}

func Add_Node_Affinity(c *gin.Context) {
	var data Node_Affinity_Input
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	fmt.Println(data)
	d := Deploy{Namespace: data.Namespace, Name: data.Name}
	fmt.Printf("d:%v\n", d)
	if err := d.add_node_affinity(data.NodeAffinity); err == nil {
		c.JSON(200, gin.H{"ok": "添加Na成功"})
	} else {
		c.JSON(500, gin.H{"fail": "添加Na失败", "error": err.Error()})
	}
}

func Delete_Pod_Anti_Affinity(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	d := Deploy{Namespace: data.Namespace, Name: data.Name}
	if err := d.delete_pod_anti_affinity(); err == nil {
		c.JSON(200, gin.H{"ok": "删除成功"})
	} else {
		c.JSON(500, gin.H{"fail": "删除失败", "error": err.Error()})
	}
}

type Pod_Anti_Affinity_Input struct {
	Namespace       string                 `json:"namespace"`
	Name            string                 `json:"name"`
	PodAntiAffinity Pod_Anti_Affinity_Data `json:"pod_anti_affinity"`
}

func Add_Pod_Anti_Affinity(c *gin.Context) {
	var data Pod_Anti_Affinity_Input
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	fmt.Println(data)
	d := Deploy{Namespace: data.Namespace, Name: data.Name}
	fmt.Printf("d:%v\n", d)
	if err := d.add_pod_anti_affinity(data.PodAntiAffinity); err == nil {
		c.JSON(200, gin.H{"ok": "添加Na成功"})
	} else {
		c.JSON(500, gin.H{"fail": "添加Na失败", "error": err.Error()})
	}
}

type Toleration_Input struct {
	Namespace  string          `json:"namespace"`
	Name       string          `json:"name"`
	Toleration Toleration_Data `json:"toleration"`
}

func Add_Toleration(c *gin.Context) {
	var data Toleration_Input
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	fmt.Println(data)
	d := Deploy{Namespace: data.Namespace, Name: data.Name}
	fmt.Printf("d:%v\n", d)
	if err := d.add_toleration(data.Toleration); err == nil {
		c.JSON(200, gin.H{"ok": "添加容忍成功"})
	} else {
		c.JSON(500, gin.H{"fail": "添加容忍失败", "error": err.Error()})
	}
}

func Delete_Toleration(c *gin.Context) {
	var data Toleration_Input
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	fmt.Println(data)
	d := Deploy{Namespace: data.Namespace, Name: data.Name}
	fmt.Printf("d:%v\n", d)
	if err := d.delete_toleration(data.Toleration); err == nil {
		c.JSON(200, gin.H{"ok": "删除容忍成功"})
	} else {
		c.JSON(500, gin.H{"fail": "删除容忍失败", "error": err.Error()})
	}
}
