package deployment

import (
	"errors"
	"fmt"

	coreV1 "k8s.io/api/core/v1"
)

type Toleration_Data struct {
	Effect            string `json:"effect"`
	Key               string `json:"key"`
	Value             string `json:"value"`
	Operator          string `json:"operator"`
	TolerationSeconds int64  `json:"tolerationSeconds"`
}

func (d *Deploy) add_toleration(data Toleration_Data) error {
	if data.Operator == "Exists" {
		data.Value = ""
	}
	var tsec *int64
	if data.TolerationSeconds == 0 {
		tsec = nil
	} else {
		tsec = &data.TolerationSeconds
	}
	toleration := coreV1.Toleration{
		Key:               data.Key,
		Effect:            coreV1.TaintEffect(data.Effect),
		Value:             data.Value,
		Operator:          coreV1.TolerationOperator(data.Operator),
		TolerationSeconds: tsec,
	}
	fmt.Printf("toleration: %v\n", toleration)
	deployment := d.get_deploy()
	t := deployment.Spec.Template.Spec.Tolerations
	t = append(t, toleration)
	deployment.Spec.Template.Spec.Tolerations = t
	return d.update(deployment)
}

func (d *Deploy) delete_toleration(data Toleration_Data) error {
	deployment := d.get_deploy()
	t := deployment.Spec.Template.Spec.Tolerations
	if len(t) == 0 {
		return errors.New("toleration is nil")
	}
	var i int = -1
	// 传入空的话1  data.TolerationSeconds = 0  *item.TolerationSeconds
	for index, item := range t {
		if item.Effect == coreV1.TaintEffect(data.Effect) &&
			item.Key == data.Key && item.Value == data.Value &&
			item.Operator == coreV1.TolerationOperator(data.Operator) {
			if (data.TolerationSeconds == 0 && item.TolerationSeconds == nil) ||
				(data.TolerationSeconds != 0 && data.TolerationSeconds == *item.TolerationSeconds) {
				i = index
				break
			}

		}
	}
	if i == -1 {
		return errors.New("tolerations not found")
	}
	t = append(t[:i], t[i+1:]...)
	deployment.Spec.Template.Spec.Tolerations = t
	return d.update(deployment)
}
