package deployment

import (
	"context"
	"fmt"
	"io"
	"strings"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	"gitee.com/cmlfxz/gin-k8s/src/middleware/log"

	"github.com/gin-gonic/gin"
	"github.com/kubernetes/dashboard/src/app/backend/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/yaml"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/dynamic"
)

// AppDeploymentFromFileSpec is a specification for deployment from file
type AppDeploymentFromFileSpec struct {
	// Name of the file
	Name string `json:"name" form:"name"`
	// Namespace that object should be deployed in
	Namespace string `json:"namespace" form:"namespace"`
	// File content
	Content string `json:"content" form:"content"`
}

type MyDeployment struct {
	// Name of the file
	Name string `json:"name" form:"name"`
	// Namespace that object should be deployed in
	Namespace string `json:"namespace" form:"namespace"`
}

func Get_Deployment_Object(c *gin.Context) {
	var spec MyDeployment
	if c.ShouldBindJSON(&spec) == nil {
		client, err := dynamic.NewForConfig(k8sConfig.K8sConfig)
		if err != nil {
			c.JSON(500, gin.H{"fail": "k8s链接失败"})
			return
		}
		deploymentRes := schema.GroupVersionResource{Group: "apps", Version: "v1", Resource: "deployments"}
		result, err := client.Resource(deploymentRes).Namespace(spec.Namespace).Get(context.TODO(), spec.Name, metaV1.GetOptions{})
		c.JSON(200, gin.H{"obj": result})
	} else {
		c.String(500, "get fail")
	}
}

func DeployAppFromFile(spec *AppDeploymentFromFileSpec) (bool, error) {
	reader := strings.NewReader(spec.Content)
	log.Log.Info("Namespace for deploy from : " + spec.Namespace)
	d := yaml.NewYAMLOrJSONDecoder(reader, 4096)
	for {
		data := &unstructured.Unstructured{}
		if err := d.Decode(data); err != nil {
			if err == io.EOF {
				return true, nil
			}
			return false, err
		}
		log.Log.Info(data)
		version := data.GetAPIVersion()
		kind := data.GetKind()
		log.Log.Info(version + " " + kind)
		gv, err := schema.ParseGroupVersion(version)
		if err != nil {
			gv = schema.GroupVersion{Version: version}
		}

		discoveryClient, err := discovery.NewDiscoveryClientForConfig(k8sConfig.K8sConfig)
		if err != nil {
			return false, err
		}
		apiResourceList, err := discoveryClient.ServerResourcesForGroupVersion(version)
		if err != nil {
			return false, err
		}
		apiResources := apiResourceList.APIResources
		//
		var resource *metaV1.APIResource
		for _, apiResource := range apiResources {
			if apiResource.Kind == kind && !strings.Contains(apiResource.Name, "/") {
				resource = &apiResource
				break
			}
		}

		if resource == nil {
			return false, fmt.Errorf("unknown resource kind: %s", kind)
		}

		dynamicClient, err := dynamic.NewForConfig(k8sConfig.K8sConfig)
		if err != nil {
			return false, err
		}
		groupVersionResource := schema.GroupVersionResource{
			Group:    gv.Group,
			Version:  gv.Version,
			Resource: resource.Name,
		}
		namespace := spec.Namespace

		if strings.Compare(spec.Namespace, "_all") == 0 {
			namespace = data.GetNamespace()
		}
		if resource.Namespaced {
			_, err = dynamicClient.Resource(groupVersionResource).Namespace(namespace).Update(context.TODO(), data, metaV1.UpdateOptions{})
		} else {
			_, err = dynamicClient.Resource(groupVersionResource).Update(context.TODO(), data, metaV1.UpdateOptions{})
		}

		if err != nil {
			return false, errors.LocalizeError(err)
		}

	}

}

func Update_Deployment(c *gin.Context) {
	var spec AppDeploymentFromFileSpec
	if c.ShouldBindJSON(&spec) == nil {
		ok, err := DeployAppFromFile(&spec)
		if ok {
			c.String(200, "update ok")
		} else {
			log.Log.Error(err.Error())
			c.String(500, "update fail")
		}

	} else {
		c.String(500, "post data error")
	}
}
