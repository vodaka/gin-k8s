package statefulset

import (
	"context"

	"gitee.com/cmlfxz/gin-k8s/src/handle/cluster"
	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/apps/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type ListData struct {
	Namespace string `json:"namespace"`
}

func Get_StatefulSet_List(c *gin.Context) {
	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.AppsV1()
	var statefulsets *v1.StatefulSetList
	if namespace == "all" || namespace == "" {
		statefulsets, _ = client.StatefulSets("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		statefulsets, _ = client.StatefulSets(namespace).List(context.TODO(), metaV1.ListOptions{})
	}

	list := make([]map[string]interface{}, 0)
	for _, statefulset := range statefulsets.Items {
		meta := statefulset.ObjectMeta
		name := meta.Name
		namespace := meta.Namespace
		create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")

		labels := meta.Labels
		spec := statefulset.Spec
		replicas := spec.Replicas
		service_name := spec.ServiceName
		template := spec.Template
		template_spec := template.Spec
		containers := template_spec.Containers
		container_list := make([]interface{}, 0)

		for _, container := range containers {
			image := container.Image
			mycontainer := map[string]interface{}{
				"image": image,
			}
			container_list = append(container_list, mycontainer)
		}
		host_network := template_spec.HostNetwork
		tolerations := template_spec.Tolerations
		// pvc_list := spec.VolumeClaimTemplates
		pvc_list := make([]map[string]interface{}, 0)
		pvc_templates := spec.VolumeClaimTemplates
		if len(pvc_templates) != 0 {
			for _, pvc_template := range pvc_templates {
				pvc_annotations := pvc_template.ObjectMeta.Annotations
				pvc_name := pvc_template.ObjectMeta.Name
				pvc_access_mode := pvc_template.Spec.AccessModes[0]
				pvc_capacity := pvc_template.Spec.Resources.Requests["storage"]
				status := pvc_template.Status
				my_pvc := map[string]interface{}{
					"pvc_name":        pvc_name,
					"pvc_access_mode": pvc_access_mode,
					"pvc_capacity":    pvc_capacity,
					"pvc_annotations": pvc_annotations,
					"status":          status,
				}
				pvc_list = append(pvc_list, my_pvc)
			}
		}

		detail := map[string]interface{}{
			"name":           name,
			"namespace":      namespace,
			"replicas":       replicas,
			"labels":         labels,
			"service_name":   service_name,
			"host_network":   host_network,
			"tolerations":    tolerations,
			"container_list": container_list,
			"pvc_list":       pvc_list,
			"create_time":    create_time,
		}
		list = append(list, detail)
	}
	c.JSON(200, list)
}

func Get_StatefulSet_Detail(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.AppsV1()
	// var statefulsets *v1.StatefulSetList
	statefulset, err := client.StatefulSets(namespace).Get(context.TODO(), name, metaV1.GetOptions{})
	if err != nil {
		c.JSON(500, gin.H{"fail": "找不到stateful相关信息"})
	} else {
		my_statefulset, _ := create_single_statefulset_object(statefulset)

		event_list, _ := cluster.Get_Resource_Event_List(namespace, "statefulset", name)
		my_statefulset["event_list"] = event_list

		c.JSON(200, my_statefulset)
	}
}

func Delete_StatefulSet(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
	} else {
		client := clientset.AppsV1()
		if namespace == "all" || namespace == "" {
			c.JSON(500, gin.H{"fail": "namespace不能为空，并且不能选择all"})
		} else {
			err := client.StatefulSets(namespace).Delete(context.TODO(), name, metaV1.DeleteOptions{})
			if err == nil {
				c.JSON(200, gin.H{"ok": "删除成功"})
			} else {
				c.JSON(500, gin.H{"fail": "删除失败"})
			}
		}
	}
}
