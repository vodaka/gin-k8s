package cluster

import (
	"context"

	"gitee.com/cmlfxz/gin-k8s/src/handle/metric"
	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	"gitee.com/cmlfxz/gin-k8s/src/util"
	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type StatInput struct {
	StatType string `json:"stat_type"`
}

func Get_Cluster_Stats(c *gin.Context) {
	var data StatInput
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()
	nodes, err := client.Nodes().List(context.TODO(), metaV1.ListOptions{})
	node_list := make([]string, 0)
	for _, node := range nodes.Items {
		meta := node.ObjectMeta
		name := meta.Name
		var schedulable bool
		if node.Spec.Unschedulable == true {
			schedulable = false
		} else {
			schedulable = true
		}
		if data.StatType == "all" {
			node_list = append(node_list, name)
		} else if data.StatType == "unschedule" && schedulable == false {
			node_list = append(node_list, name)
		} else {
			// 默认只统计schedule的数据
			if schedulable == true {
				node_list = append(node_list, name)
			}
		}
	}
	var total_cpu, total_cpu_usage float64 = 0, 0
	var total_memory, total_memory_usage float64 = 0, 0

	var total_disk float64 = 0
	var total_pod int64 = 0
	list := make([]map[string]interface{}, 0)
	if len(node_list) > 0 {
		for _, name := range node_list {
			capacity := node_capacity(name)
			cpu := capacity.Cpu().MilliValue()
			memory := util.Handle_Memory(capacity.Memory().Value())
			pod := capacity.Pods().Value()
			disk := util.Handle_Disk(capacity.StorageEphemeral().Value())

			usage, err := metric.Node_Usage(name)
			if err != nil {
				c.JSON(500, gin.H{"fail": "get node usage fail"})
				return
			}
			cpu_usage := usage.Cpu
			memory_usage := usage.Memory
			//集群CPU
			total_cpu += float64(cpu)
			total_cpu_usage += cpu_usage
			//集群内存
			total_memory += memory
			total_memory_usage += memory_usage
			//磁盘总量
			total_disk += disk
			//Pod
			total_pod += pod
		}
		stat := map[string]interface{}{
			"cpu_total":    total_cpu,
			"cpu_usage":    total_cpu_usage,
			"memory_total": total_memory,
			"memory_usage": total_memory_usage,
			"pod_total":    total_disk,
			"disk":         total_pod,
		}
		list = append(list, stat)
	}

	c.JSON(200, list)
}

func node_capacity(name string) v1.ResourceList {
	clientset, _ := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	client := clientset.CoreV1()
	node, _ := client.Nodes().Get(context.TODO(), name, metaV1.GetOptions{})
	return node.Status.Capacity
}
