package hpa

import (
	"context"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/autoscaling/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type ListData struct {
	Namespace string `json:"namespace"`
}

func Get_Hpa_List(c *gin.Context) {
	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.AutoscalingV1()
	var hpas *v1.HorizontalPodAutoscalerList
	if namespace == "all" || namespace == "" {
		hpas, _ = client.HorizontalPodAutoscalers("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		hpas, _ = client.HorizontalPodAutoscalers(namespace).List(context.TODO(), metaV1.ListOptions{})
	}
	list := make([]map[string]interface{}, 0)
	for _, hpa := range hpas.Items {
		detail := Detail(&hpa)
		list = append(list, detail)
	}
	c.JSON(200, list)
}

type UpdateData struct {
	Namespace   string `json:"namespace"`
	Name        string `json:"name"`
	MinReplicas int32  `json:"minReplicas"`
	MaxReplicas int32  `json:"maxReplicas"`
}

func Update_Hpa_Replicas(c *gin.Context) {
	var data UpdateData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
	} else {
		clientset, _ := kubernetes.NewForConfig(k8sConfig.K8sConfig)
		client := clientset.AutoscalingV1()

		hpa, err := client.HorizontalPodAutoscalers(data.Namespace).Get(context.TODO(), data.Name, metaV1.GetOptions{})
		if err == nil {
			hpa.Spec.MinReplicas = &data.MinReplicas
			hpa.Spec.MaxReplicas = data.MaxReplicas
			_, err := client.HorizontalPodAutoscalers(data.Namespace).Update(context.TODO(), hpa, metaV1.UpdateOptions{})
			if err == nil {
				c.JSON(200, gin.H{"ok": "更新成功"})
			} else {
				c.JSON(500, gin.H{"fail": "更新失败"})
			}

		} else {
			c.JSON(500, gin.H{"fail": "查找hpa出错"})
		}

	}

}
func Delete_Hpa(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
	} else {
		client := clientset.AutoscalingV1()
		if namespace == "all" || namespace == "" {
			c.JSON(500, gin.H{"fail": "namespace不能为空，并且不能选择all"})
		} else {
			err := client.HorizontalPodAutoscalers(namespace).Delete(context.TODO(), name, metaV1.DeleteOptions{})
			if err == nil {
				c.JSON(200, gin.H{"ok": "删除成功"})
			} else {
				c.JSON(500, gin.H{"fail": "删除失败"})
			}
		}
	}
}
