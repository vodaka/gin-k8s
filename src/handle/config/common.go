package config

type InputData struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`
}
