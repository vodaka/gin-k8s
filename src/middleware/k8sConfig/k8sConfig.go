package k8sConfig

import (
	"fmt"
	"os"
	"path"

	"encoding/base64"
	"time"

	"gitee.com/cmlfxz/gin-k8s/src/lib/gredis"
	"gitee.com/cmlfxz/gin-k8s/src/model"
	"github.com/gin-gonic/gin"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

var K8sConfig *restclient.Config

func GetDbConfig(cluster_name string) string {
	var cluster model.Cluster
	model.Db.First(&cluster, "cluster_name=?", cluster_name)

	// fmt.Printf("%#v\n", cluster)
	return cluster.Cluster_Config
}

func GetCacheConfig(cluster_name string) string {
	key := cluster_name + "_config_file"
	client := gredis.NewRedisClient(0)
	if client == nil {
		fmt.Println("redis连不上")
		return ""
	}
	k8s_conf, err := client.Get(key).Result()
	if err != nil {
		fmt.Println("从缓存获取配置失败")
		return ""
	}
	return k8s_conf
}

func SetCacheConfig(cluster_name string, cluster_config string) bool {
	key := cluster_name + "_config_file"
	client := gredis.NewRedisClient(0)
	if client == nil {
		fmt.Println("redis连不上")
		return false
	}
	client.Set("cluster_name", cluster_name, time.Duration(3*60*60*time.Second))
	client.Set(key, cluster_config, 0)

	return true
}

func SetConfig(c *gin.Context) {
	fmt.Println("设置k8s配置")
	//改造成从redis读取
	client := gredis.NewRedisClient(0)
	if client == nil {
		panic("redis连不上")
	}
	//获取header
	cluster_name := c.GetHeader("cluster_name")
	if cluster_name == "" {
		fmt.Println("请求不带cluster_name")
	}
	cache_name, _ := client.Get("cluster_name").Result()
	// key := cluster_name + "_config_file"

	var k8s_conf string
	if cluster_name == cache_name {
		// fmt.Println("从缓存获取配置")
		k8s_conf = GetCacheConfig(cluster_name)
	} else {
		// fmt.Println("从数据库获取配置")
		k8s_conf = GetDbConfig(cluster_name)
		if k8s_conf != "" {
			//设置redis缓存
			SetCacheConfig(cluster_name, k8s_conf)
		}
	}
	if k8s_conf == "" {
		fmt.Println("获取k8s配置失败")
	}

	//[]byte
	confByte, err := base64.URLEncoding.DecodeString(k8s_conf)
	if err != nil {
		fmt.Println("配置解码失败")
	}
	confString := string(confByte)
	//写入本地文件
	dir, _ := os.Getwd()
	filname := cluster_name + ".config"
	dst := path.Join(dir, filname)
	dstFile, err := os.Create(dst)
	if err != nil {
		fmt.Println("创建配置文件失败")
	}
	defer dstFile.Close()
	dstFile.WriteString(confString)

	// fmt.Println(dst)

	config, err := clientcmd.BuildConfigFromFlags("", dst)
	if err != nil {
		fmt.Println(err)
		c.JSON(500, gin.H{"fail": "设置k8s配置失败"})

	}
	K8sConfig = config
}

func SetK8sConfig() gin.HandlerFunc {
	return func(c *gin.Context) {
		SetConfig(c)
		c.Next()
	}
}
