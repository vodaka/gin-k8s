package log

import (
	"fmt"
	"os"
	"path"
	"runtime"
	"time"

	"github.com/gin-gonic/gin"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
)

var (
// logFilePath = "./logs/"
// logFileName = "gin.log"
)
var Log *logrus.Logger

func Logger() *logrus.Logger {
	// now := time.Now()

	logFilePath := ""
	if dir, err := os.Getwd(); err == nil {
		logFilePath = path.Join(dir, "logs")
	}

	if _, err := os.Stat(logFilePath); err != nil {
		if err := os.Mkdir(logFilePath, os.ModePerm); err != nil {
			fmt.Println("创建目录失败")
			os.Exit(-1)
		} else {
			fmt.Println("创建目录成功")
		}
	}
	// logFileName := now.Format("2006-01-02") + ".log"

	logFileName := "access" + ".log"

	fileName := path.Join(logFilePath, logFileName)

	fmt.Println(fileName)
	if _, err := os.Stat(fileName); err != nil {
		if _, err := os.Create(fileName); err != nil {
			fmt.Println(err.Error())
		}
	}
	src, err := os.OpenFile(fileName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		fmt.Println("err", err)
	}

	//实例化
	logger := logrus.New()

	logger.Out = src

	logger.SetLevel(logrus.DebugLevel)

	logger.SetFormatter(&logrus.TextFormatter{
		TimestampFormat: "2006-02-02 15:04:05",
	})

	//设置日志切割
	//设置日志切割
	if runtime.GOOS != "windows" {
		logWriter, _ := rotatelogs.New(
			//分割后的文件名
			// path.Join(logFilePath, "access-"+"%Y%m%d-%H%M.log"),
			path.Join(logFilePath, "access-"+"%Y%m%d.log"),
			//生成软连指向最新的日志文件
			rotatelogs.WithLinkName(fileName),

			//设置最大的保存时间
			rotatelogs.WithMaxAge(time.Duration(3*24)*time.Hour),

			//设置日志切割时间间隔(1天)
			rotatelogs.WithRotationTime(time.Duration(24)*time.Hour),
			// rotatelogs.WithRotationTime(time.Duration(1)*time.Minute),
		)
		writeMap := lfshook.WriterMap{
			logrus.InfoLevel:  logWriter,
			logrus.FatalLevel: logWriter,
			logrus.DebugLevel: logWriter,
			logrus.WarnLevel:  logWriter,
			logrus.ErrorLevel: logWriter,
			logrus.PanicLevel: logWriter,
		}
		lfHook := lfshook.NewHook(writeMap, &logrus.TextFormatter{
			TimestampFormat: "2006-01-02 15:04:05",
		})
		// 新增 Hook
		logger.AddHook(lfHook)
	}

	Log = logger
	return logger
}

func LoggerToFile() gin.HandlerFunc {
	fmt.Println("使用日志中间件")
	logger := Logger()

	return func(c *gin.Context) {
		startTime := time.Now()

		c.Next()

		endTime := time.Now()

		latencyTime := endTime.Sub(startTime)

		requestMethod := c.Request.Method

		requestUri := c.Request.RequestURI

		statusCode := c.Writer.Status()

		clientIp := c.ClientIP()

		c.RemoteIP()

		logger.Infof("| %15s | %s | %s | %3d | %13v |",
			clientIp,
			requestMethod,
			requestUri,
			statusCode,
			latencyTime,
		)
	}
}

// 日志记录到 ES
func LoggerToES() gin.HandlerFunc {
	return func(c *gin.Context) {
	}
}
