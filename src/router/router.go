package router

import (
	"gitee.com/cmlfxz/gin-k8s/src/handle/auth"
	"gitee.com/cmlfxz/gin-k8s/src/handle/cluster"
	"gitee.com/cmlfxz/gin-k8s/src/handle/config"
	"gitee.com/cmlfxz/gin-k8s/src/handle/daemonset"
	"gitee.com/cmlfxz/gin-k8s/src/handle/deployment"
	"gitee.com/cmlfxz/gin-k8s/src/handle/hpa"
	"gitee.com/cmlfxz/gin-k8s/src/handle/istio"
	"gitee.com/cmlfxz/gin-k8s/src/handle/namespace"
	"gitee.com/cmlfxz/gin-k8s/src/handle/node"
	"gitee.com/cmlfxz/gin-k8s/src/handle/pod"
	"gitee.com/cmlfxz/gin-k8s/src/handle/pv"
	"gitee.com/cmlfxz/gin-k8s/src/handle/service"
	"gitee.com/cmlfxz/gin-k8s/src/handle/statefulset"
	"gitee.com/cmlfxz/gin-k8s/src/handle/storage"
	"gitee.com/cmlfxz/gin-k8s/src/middleware/cors"
	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	"gitee.com/cmlfxz/gin-k8s/src/middleware/log"
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.Default()
	r.Use(cors.Cors())
	r.Use(log.LoggerToFile())
	r.Use(k8sConfig.SetK8sConfig())

	k8s := r.Group("/k8s")
	{
		k8s.POST("/get_deployment_list", deployment.Get_Deployment_List)
		k8s.POST("/update_deployment_object", deployment.Update_Deployment)
		k8s.POST("/get_deployment_object", deployment.Get_Deployment_Object)
		k8s.POST("/get_problem_deployments", deployment.Get_Problem_Deployment)
		k8s.POST("/get_deployment_detail", deployment.Get_Deployment_Detail)
		k8s.POST("/get_deployment_toleration", deployment.Get_Deployment_Toleration)
		k8s.POST("/get_deployment_replicas", deployment.Get_Deployment_Replicas)
		k8s.POST("/delete_deploy", deployment.Delete_Deployment)
		k8s.POST("/update_replicas", deployment.Update_Replicas)
		k8s.POST("/delete_node_affinity", deployment.Delete_Node_Affinity)
		k8s.POST("/add_node_affinity", deployment.Add_Node_Affinity)
		k8s.POST("/delete_pod_anti_affinity", deployment.Delete_Pod_Anti_Affinity)
		k8s.POST("/add_pod_anti_affinity", deployment.Add_Pod_Anti_Affinity)
		k8s.POST("/add_toleration", deployment.Add_Toleration)
		k8s.POST("/delete_toleration", deployment.Delete_Toleration)
		//auth
		k8s.POST("/get_service_account_list", auth.Get_Service_Account_List)
		k8s.POST("/delete_service_account", auth.Delete_Service_Account)

		//cluster
		k8s.POST("/get_event_list", cluster.Get_Event_List)
		k8s.POST("/get_cluster_stats", cluster.Get_Cluster_Stats)
		//daemonset
		k8s.POST("/get_daemonset_list", daemonset.Get_DaemonSet_List)
		k8s.POST("/delete_daemonset", daemonset.Delete_DaemonSet)
		//hpa
		k8s.POST("/get_hpa_list", hpa.Get_Hpa_List)
		k8s.POST("/delete_hpa", hpa.Delete_Hpa)
		k8s.POST("/update_hpa_replicas", hpa.Update_Hpa_Replicas)

		//pod
		k8s.POST("/get_pod_detail", pod.Get_Pod_Detail)
		k8s.POST("/get_namespaced_pod_list", pod.Get_Namespaced_Pod_List)
		k8s.POST("/get_problem_pod_list", pod.Get_Problem_Pod_List)
		k8s.POST("/delete_pod", pod.Delete_POD)
		k8s.POST("/get_pod_list_by_node", pod.Get_Pod_List_By_Node)
		k8s.POST("/get_container_name", pod.Get_Container_Name)
		k8s.POST("/get_pod_log", pod.Get_Pod_Log)
		//service
		k8s.POST("/get_service_list", service.Get_Service_List)
		k8s.POST("/get_ingress_list", service.Get_Ingress_List)
		k8s.POST("/delete_service", service.Delete_Service)
		k8s.POST("/delete_ingress", service.Delete_Ingress)

		//istio
		k8s.POST("/get_gateway_list", istio.Get_Gateway_List)

		//config
		k8s.POST("/get_configmap_list", config.Get_ConfigMap_List)
		k8s.POST("/get_cm_detail", config.Get_ConfigMap_Detail)
		k8s.POST("/get_secret_list", config.Get_Secret_List)
		k8s.POST("/get_secret_detail", config.Get_Secret_Detail)
		k8s.POST("/delete_configmap", config.Delete_ConfigMap)
		k8s.POST("/delete_secret", config.Delete_Secret)
		//statefulset
		k8s.POST("/get_statefulset_list", statefulset.Get_StatefulSet_List)
		k8s.POST("/get_statefulset_detail", statefulset.Get_StatefulSet_Detail)
		k8s.POST("/delete_statefulset", statefulset.Delete_StatefulSet)
		//node
		k8s.POST("/get_node_list", node.Get_Node_List)
		k8s.POST("/update_node", node.Update)
		k8s.POST("/get_node_name_list", node.Get_Node_Name_List)
		k8s.POST("/get_node_stat", node.Get_Node_Stat)
		//storgae
		k8s.POST("/get_storageclass_list", storage.Get_StorageClass_List)
		k8s.POST("/get_pvc_list", storage.Get_PVC_List)
		k8s.POST("/select_pvc", storage.Select_PVC)
		k8s.POST("/delete_pvc", storage.Delete_PVC)
		//pv
		k8s.POST("/get_pv_list", pv.Get_PV_List)
		k8s.POST("/select_pv", pv.Select_PV)
		k8s.POST("/create_pv", pv.Create_PV)
		k8s.POST("/delete_pv", pv.Delete_PV)
		k8s.POST("/delete_multi_pv", pv.Delete_Multi_PV)

		//namespace
		k8s.POST("/create_namespace", namespace.Create_Namespace)
		k8s.POST("/delete_namespace", namespace.Delete_Namespace)
		k8s.POST("/update_namespace", namespace.Update_Namespace)
		k8s.POST("/get_namespace_list", namespace.Get_Namespace_List)
		k8s.POST("/get_namespace_name_list", namespace.Get_Namespace_Name_List)
		k8s.POST("/get_all_namespace_resources", namespace.Get_ALL_Namespace_Resources)
	}
	return r
}
