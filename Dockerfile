FROM  myhub.mydocker.com/base/debian-golang:1.16

ENV GO111MODULE=on \
    GOPROXY=https://goproxy.cn,direct \
    GIN_MODE=release

WORKDIR /opt/microservice

COPY . .

RUN go build .

EXPOSE 8090

ENTRYPOINT ["./gin-k8s"]